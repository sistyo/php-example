FROM sistyo/nginx-php-fpm:latest
ADD src/. /var/www/html/
RUN chmod -R 755 /var/www/html/*
RUN chmod 777 /var/www/html/uploads
