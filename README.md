This is a simple Docker-PHP application example for educational purpose only

### Content:
1. simple index page
2. upload demo
3. mysql demo

to use mysql demo, you need mysq db. create one using docker command below:
```shell
docker run -d --name mariadb -e MARIADB_USER=demo -e MARIADB_PASSWORD=demo1234 -e MARIADB_ROOT_PASSWORD=changeme  mariadb:latest
```
